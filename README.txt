
-- SUMMARY --

PDF Parser allow user extract content from a PDF and add it to the Body field.


-- REQUIRENENTS --

PDF Parser require this module enabled
 - Zend

-- INSTALLATION --

1. Extract pdf_parser on your module directory (ex. sites/all/modules)

-- CONFIGURATION --

After installed and enable the module, add a new field in Stucture -> Content Types -> {Type} -> Manage field.

The new added field must accept PDF and you've to enable the PDF Parser for this field.

Add a new content and upload a PDF file, you'll see a [extract text] link that gonna open a dialog and extract the text.

If everything works well, you should see the text in the body field.

-- TROUBLESHOOTING --

-- FAQ --

-- CONTACT --

Current maintainers:
* Yvan Marques (yvmarques) - http://drupal.org/user/298685