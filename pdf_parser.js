(function($){
  Drupal.PDFParser = {
    extract: function(uri) {
      $('body').append('<div id="pdf-parser-dialog">Extracting…</div>');
      $('#pdf-parser-dialog').dialog({
        buttons: {
          "Close": function(){
            $(this).dialog('close');
          }
        },
        modal: true,
        title: "Extracting...",
        close: function(){
          $(this).dialog('destroy');
        },
        open: function(){
          $.get(Drupal.settings.basePath + 'pdf_parser/extract', {'uri': uri}, function(data){
            $('textarea[id*="body"]').val(data);
            setTimeout(function(){$('#pdf-parser-dialog').dialog('close');}, 3000);
          });
        },
        resizable: false,
      });
    },
  };
})(jQuery);